import css from '../css/index.module.css'
import Produits from '../components/Produits/Produits'
import Footer from '../components/Footer/Footer'
import Commande from '../components/Commande/Commande'

const Home = () => (
  <div className={css.container}>
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <div className={css.first_view}>
      <img src="/logo/logo_400.png" alt="Logo"className={css.logo} />
      <div className={css.buttons_container}>
        <a href="#produits">
          <p className={css.anchor_button}>Découvrir les produits</p>
        </a>
        <a href="#commande">
          <p className={css.anchor_button}>Commander</p>
        </a>
      </div>
    </div>
      
    <Produits />

    <Commande />

    <Footer />
  </div>
)

export default Home
