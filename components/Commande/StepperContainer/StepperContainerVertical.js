import React, { Component } from 'react'
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent'
import Button from '@material-ui/core/Button';
import Bases from './Steps/Bases/Bases'
import Aromes from './Steps/Aromes/Aromes';
import Volumes from './Steps/Volumes/Volumes'
import Panier from './Steps/Panier/Panier';
import Livraison from './Steps/Livraison/Livraison'
import Coordonnees from './Steps/Coordonnees/Coordonnees'
import produits_specs from '../../../data/produits_specs.json'
import Confirmation from './Steps/Confirmation/Confirmation'
import { loadFirebase } from '../../../lib/db'
import Enregistree from './Steps/Enregistree/Enregistree';


export default class StepperContainerVertical extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      steps: ["Bases","Arômes", "Volume", "Panier", "Livraison", "Coordonnées", "Confirmation"],
      selection: {
        base: "",
        arome: "",
        volume: "",
        prix: 0,
        quantite: 1
      },
      commande: {
        id:"",
        produits: [],
        prix_produits: 0,
        volume_produits: 0,
        livraison: 0,
        expedition: 0,
        prix_expedition: 0,
        prix_commande: 0,
        statut_commande: "En cours de préparation",
        coordonnees: {
          nom: "",
          prenom: "",
          tel: "",
          insta: "",
          adresse: {
            numero: "",
            rue: "",
            code_postal: "",
            ville: ""
          }
        },
        date: "",
      },
      completed: false,
      loading: true,
      error: false
    }

    this.handleNext = this.handleNext.bind(this)
    this.handleBack = this.handleBack.bind(this)
    this.handleAddProduct = this.handleAddProduct.bind(this)
    this.handleDeleteProduct = this.handleDeleteProduct.bind(this)
    this.handleSelection = this.handleSelection.bind(this)
    this.currentSelection = this.currentSelection.bind(this)
    this.updateQuantite = this.updateQuantite.bind(this)
    this.calculCommande = this.calculCommande.bind(this)
    this.updateLivraison = this.updateLivraison.bind(this)
    this.handleCoordonnees = this.handleCoordonnees.bind(this)
    this.handleDatabase = this.handleDatabase.bind(this)
    this.handleConfirmation = this.handleConfirmation.bind(this)
  }
  
  getStepContent(step) {
    switch(step) {
      case 0: return <Bases handleSelection={this.handleSelection} currentSelection={this.currentSelection} />
      case 1: return <Aromes handleSelection={this.handleSelection} currentSelection={this.currentSelection} />
      case 2: return <Volumes handleSelection={this.handleSelection} currentSelection={this.currentSelection} />
      case 3: return <Panier commande={this.state.commande} handleDeleteProduct={this.handleDeleteProduct} updateQuantite={this.updateQuantite} calculCommande={this.calculCommande} />
      case 4: return <Livraison commande={this.state.commande} updateLivraison={this.updateLivraison} />
      case 5: return <Coordonnees commande={this.state.commande} handleCoordonnees={this.handleCoordonnees} />
      case 6: return <Confirmation commande={this.state.commande} />
      default: "Etape inconnue"
    }
  }

  handleNext() {
    const currentStep = this.state.activeStep
    const commande = this.state.commande
    this.setState({activeStep: currentStep + 1})

    if(currentStep + 1 === 3) {
      commande.produits.push(this.state.selection)
      this.setState({commande: commande})
    } else if (currentStep === this.state.steps.length - 1) {
      this.handleConfirmation()
    }
  }
  
  updateQuantite(index, value) {
    const commande = this.state.commande
    commande.produits[index].quantite = value

    this.setState({commande: commande})

    this.calculCommande()
  }

  handleBack() {
    const currentStep = this.state.activeStep
    this.setState({activeStep: currentStep - 1})
  }


  handleAddProduct() {
    const defaultSelection = {
      base: "",
      arome: "",
      volume: "",
      prix: 0,
      quantite: 1
    }
    this.setState({
      activeStep: 0,
      selection: defaultSelection
    })
  }

  handleDeleteProduct(index) {
    const commande = this.state.commande
    commande.produits.splice(index, 1)
    this.setState({commande: commande})
    this.calculCommande()
    if (commande.produits.length === 0) {
      this.handleAddProduct()
    }
  }

  calculCommande() {
    const commande = this.state.commande
    const prix_produits = []
    const volume_produits = []
    let prix_total = 0
    let volume_total = 0
    let expedition = 0
    let prix_expedition = 0
    let prix_commande

    const reducer = (accumulator, item) => accumulator + item

    if(commande.produits.length > 0) {
      commande.produits.map((produit, index) => (
        prix_produits.splice(index, 1, produit.prix * produit.quantite),
        volume_produits.splice(index, 1, produit.volume * produit.quantite)
      ))

      prix_total = prix_produits.reduce(reducer)
      volume_total = volume_produits.reduce(reducer)
        //Calcul prix expedition
      if (volume_total === 10) {
        expedition = 1
        prix_expedition = produits_specs.expedition[1]
      } else if (volume_total <= 50) {
        expedition = 2
        prix_expedition = produits_specs.expedition[2]
      } else if (volume_total > 50) {
        expedition = 3
        prix_expedition = produits_specs.expedition[3]
      } else {
        expedition = 0
        prix_expedition = produits_specs.expedition[0]
      }
    } else {
      prix_total = 0
      volume_total = 0
      expedition = 0
      prix_expedition = 0
    }

    //Calcul prix de la commande 
    if (commande.livraison === 0) {
      prix_commande = prix_total
    } else {
      prix_commande = prix_total + prix_expedition
    }

    commande.prix_produits = prix_total
    commande.volume_produits = volume_total
    commande.expedition = expedition
    commande.prix_expedition = prix_expedition
    commande.prix_commande = prix_commande
    this.setState({commande: commande})
  }

  updateLivraison(value) {
    let commande = this.state.commande
    commande.livraison = value
    this.setState({commande: commande})
    this.calculCommande()
  }

  handleSelection(key, value) {
    const selection = this.state.selection
    // selection[key] = value

    if(selection[key] === "" || selection[key] === 0 || selection[key] !== value) {
      selection[key] = value
      this.setState({
        selection: selection
      })
    } else if(selection[key] === value) {
      selection[key] = ""
      this.setState({
        selection: selection
      })
    }
  }

  isDisabled(step) {
    const selection = this.state.selection
    if(step === 0 && selection.base !== "" || step === 1 && selection.arome !== "" || step === 2 && selection.volume !== "") {
      return false
    } else if (step >= 3) {
      return false 
    }else return true
  }

  currentSelection(key) {
    return this.state.selection[key]
  }

  handleCoordonnees(key, value) {
    const commande = this.state.commande
    commande.coordonnees[key] = value
    this.setState({commande: commande})
  }

  async handleDatabase(data) {
    let firebase = await loadFirebase()
    let db = firebase.firestore()

    db.collection("commandes").doc(data.id).set(data)
    .then(this.setState({
      completed: true, 
      loading: false
    }))
    .catch(error => console.log("Erreur :", error), this.setState({
      error: true, 
      loading: false,
    }))
  }

  handleConfirmation() {
    let commande = this.state.commande 
    const date = new Date()
    const commande_id = `${date.getDate()}${date.getMonth() + 1}-${Math.floor(Math.random() * 100)}` 
    commande.date = date
    commande.id = commande_id
    this.setState({commande: commande})

    this.handleDatabase(commande)
  }

  render() {
    const {activeStep, steps} = this.state
    return (
      <div>
        <Stepper activeStep={activeStep} orientation="vertical" >
          {
            steps.map((label, index) => (
              <Step key={label} >
                <StepLabel> {label} </StepLabel>
                <StepContent>
                  <div>
                    {this.getStepContent(activeStep)}
                    <div style={{marginTop: 10}}>
                      {
                        activeStep !== 3 && activeStep !== 0 ? (
                          <Button disabled={activeStep === 0 || activeStep === 3} onClick={this.handleBack}>Retour</Button>
                        ) : null
                      }
                      {
                        activeStep === 3 ? (
                          <Button onClick={this.handleAddProduct} >Ajouter un produit</Button>
                        ) : null
                      }
                      {
                        activeStep === steps.length - 1 ? (
                          <Button onClick={this.handleNext} disabled={this.isDisabled(activeStep)} color="primary" variant="contained" style={{margin: 10}}>
                            Confirmer la commande
                          </Button>
                        ) : activeStep <= 2 ? (
                          <Button onClick={this.handleNext} disabled={this.isDisabled(activeStep)} style={{margin: 10}}>
                            Suivant
                          </Button>
                        ) : (
                          <Button onClick={this.handleNext} disabled={this.isDisabled(activeStep)} style={{margin: 10}}>
                            Continuer
                          </Button>
                        )
                      }
                    </div>
                  </div>
                </StepContent>
              </Step>
            ))
          }
        </Stepper>
        {
          activeStep === steps.length && (
            <Enregistree completed={this.state.completed} error={this.state.error} loading={this.state.loading} commande_id={this.state.commande.id} />
          )
        }
  
  
      </div>
    )
  }
  
}