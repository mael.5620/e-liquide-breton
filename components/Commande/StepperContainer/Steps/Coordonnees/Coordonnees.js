import React, { useState } from 'react'
import css from './Coordonnees.module.css'
import AdresseForm from './AdresseForm/AdresseForm'

const Coordonnees = (props) => (
  <div className={css.container} >
    <div className={css.form_container} >
      <input 
        type="text" 
        placeholder="Nom"
        className={css.input} 
        onChange={e => props.handleCoordonnees("nom", e.target.value)}
      />
      <input 
        type="text" 
        placeholder="Prénom"
        className={css.input} 
        onChange={e => props.handleCoordonnees("prenom", e.target.value)}
      />
      <input 
        type="text" 
        placeholder="Insta"
        className={css.input} 
        onChange={e => props.handleCoordonnees("insta", e.target.value)}
      />
      <input 
        type="text" 
        placeholder="Tel"
        className={css.input} 
        onChange={e => props.handleCoordonnees("tel", e.target.value)}
      />
    </div>
    {
      props.commande.livraison === 1 ? <AdresseForm commande={props.commande} handleCoordonnees={props.handleCoordonnees} /> : null
    }
  </div>
)

export default Coordonnees