import React from 'react'
import css from './AdresseForm.module.css'

const AdresseForm = (props) => {
  
  const handleChange = (key, value) => {
    let adresse = props.commande.coordonnees.adresse
    adresse[key] = value
    return props.handleCoordonnees("adresse", adresse)
  }

  return (
    <div className={css.container} >
      <p className={css.title} >Adresse de livraison</p>
      <div className={css.form_container} >
        <input 
          type="text" 
          placeholder="N° de voie"
          className={css.input} 
          onChange={e => handleChange("numero", e.target.value)}
        />
        <input 
          type="text" 
          placeholder="Rue"
          className={css.input} 
          onChange={e => handleChange("rue", e.target.value)}
        />
        <input 
          type="text" 
          placeholder="Ville"
          className={css.input} 
          onChange={e => handleChange("ville", e.target.value)}
        />
        <input 
          type="text" 
          placeholder="Code Postal"
          className={css.input} 
          onChange={e => handleChange("code_postal", e.target.value)}
        />
      </div>
    </div>
)}

export default AdresseForm