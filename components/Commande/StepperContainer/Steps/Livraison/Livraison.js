import React from 'react'
import css from './Livraison.module.css'

const Livraison = (props) => (
  <div className={css.container} >
    <label>
      <input 
      type="radio" 
      value={0}
      onChange={() => props.updateLivraison(0)} 
      checked={props.commande.livraison === 0} 
      className={css.radio_button}
      />
      Remise en main propre
    </label> <br/>
    <label>
      <input 
      type="radio" 
      value={1}
      onChange={() => props.updateLivraison(1)} 
      checked={props.commande.livraison === 1} 
      className={css.radio_button}
      />
      Expédition pour {props.commande.prix_expedition} €
    </label>

    <p className={css.montant} >Montant de la commande : {props.commande.prix_commande} € </p>
  </div>
)

export default Livraison  