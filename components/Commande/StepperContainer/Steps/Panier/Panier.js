import React, {useEffect} from 'react'
import css from './Panier.module.css'

const Panier = (props) => {
  useEffect(() => props.calculCommande(), [props.commande])
  return (
    <div className={css.container} >
      <div className={css.product_list} >
        {
          props.commande.produits.map((item, index) => (
            <div className={css.list_item} >
              <p className={css.text} > {item.arome} </p>
              <p className={css.text} > {item.base} </p>
              <p className={css.text} > {item.volume} ml</p>
              <p className={css.text} > {item.prix} €</p>
              <span>
                <p className={css.text} >Quantité : </p>
                <select name="quantite" id="" value={item.quantite} onChange={e => props.updateQuantite(index, e.target.value) } >
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
              </span>
              <button onClick={() => props.handleDeleteProduct(index)} className={css.remove_button} > <img src="/icons/bin.png" alt=""/> </button>
            </div>
          ))
        }
      </div>
      <div className={css.footer} >
        <p className={css.total} >Prix total : {props.commande.prix_produits} € </p>
      </div>
    </div>
  )
}
export default Panier