import React from 'react'
import produits from '../../../../../data/produits_specs.json'
import css from './Volumes.module.css'
import VolumeCardMini from './VolumeCardMini/VolumeCardMini'


const Volumes = (props) => {
  return (
    <div className={css.container} >
      {
        produits.tarifs.map(item => (
          <VolumeCardMini
            image={item.image}
            prix={item.prix}
            volume={item.volume}
            handleSelection={props.handleSelection}
            currentSelection={props.currentSelection}
          />
        ))
      }
    </div>
  )
}

export default Volumes