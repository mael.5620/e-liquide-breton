import React, { useEffect, useState } from "react"
import css from './VolumeCardMini.module.css'
import classNames from 'classnames'

const VolumeCardMini = (props) => {
  const [isSelected, setSelected] = useState(props.currentSelection)
  useEffect(() => props.currentSelection('volume') === props.volume ? setSelected(true) : setSelected(false), [props.currentSelection('volume')])
  return (
    <div className={classNames(css.card_container, isSelected ? css.selected : null)} onClick={() => (
      props.handleSelection('volume', props.volume),
      props.handleSelection('prix', props.prix)
    )} >
      <img src={props.image} alt="" className={css.image} />
      <p className={css.volume} > {props.volume} ml </p>
      <p className={css.prix} > {props.prix} € </p>
    </div>
  )
}
export default VolumeCardMini