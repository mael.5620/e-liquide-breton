import React from 'react'
import produits from '../../../../../data/produits_specs.json'
import css from './Bases.module.css'
import BaseCardMini from './BaseCardMini/BaseCardMini'


const Bases = (props) => {
  return (
    <div className={css.container} >
      {
        produits.bases.map(item => (
          <BaseCardMini
            image={item.image}
            titre={item.titre}
            nicotine={item.booster}
            handleSelection={props.handleSelection}
            currentSelection={props.currentSelection}
          />
        ))
      }
    </div>
  )
}

export default Bases