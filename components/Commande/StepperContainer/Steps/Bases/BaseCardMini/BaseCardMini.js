import React, { useState, useEffect } from "react"
import css from './BaseCardMini.module.css'
import classNames from 'classnames'

const BaseCard = ({image, titre, nicotine, handleSelection, currentSelection}) => {
  const [isSelected, setSelected] = useState(currentSelection)
  useEffect(() => currentSelection('base') === titre ? setSelected(true) : setSelected(false), [currentSelection('base')])
  return (
    <div className={classNames(css.card_container, isSelected ? css.selected : null)} onClick={() => handleSelection('base', titre)} >
      <img src={image} alt="" className={css.image} />
      <p className={css.titre}> {titre} </p>
      <p className={css.nicotine} > {nicotine} </p>
    </div>
)}

export default BaseCard