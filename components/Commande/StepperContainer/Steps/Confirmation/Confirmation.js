import React from 'react'
import css from './Confirmation.module.css'

const Confirmation = (props) => (
  <div className={css.container} >
    <p className={css.title} >Récapitulatif</p>
    <p>Nombre de produit(s) : {props.commande.produits.length} </p>
    <p>Montant de la commande : {props.commande.prix_commande} € </p>
    {
      props.commande.livraison === 1 ? (
        <div>
          <p>Adresse de livraison : </p>
          <p> {props.commande.coordonnees.nom} {props.commande.coordonnees.prenom} </p>
          <p>{props.commande.coordonnees.adresse.numero} {props.commande.coordonnees.adresse.rue}</p>
          <p>{props.commande.coordonnees.adresse.code_postal} {props.commande.coordonnees.adresse.ville}</p>
        </div>
      ) : null
    }
    <p className={css.title} >Paiement</p>
    {
      props.commande.livraison === 0 ? (
        <p>Le paiement de la commande se fera en espèce à la remise en main propre. <br/> Merci d'envoyer un message à <a className={css.link} target="_blank" href="https://www.instagram.com/eliquidebreton/?hl=fr" >@eliquidebreton</a> pour connaître la procédure à suivre.</p>
      ) : (
        <p>Le paiement de la commande se fera par virement Paypal (entre particulier). <br/> Merci d'envoyer un message sur Instagram à <a className={css.link} target="_blank" href="https://www.instagram.com/eliquidebreton/?hl=fr" >@eliquidebreton</a> pour connaître la procédure à suivre.</p>
      )
    }
  </div>
)

export default Confirmation