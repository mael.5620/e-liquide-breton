import React from 'react'
import css from './Enregistree.module.css'

const Enregistree = (props) => (
  <div className={css.container} >
    {
    props.loading ? (
      <p>Loading</p>
    ) : (
      <div>
        <p className={css.merci} >Merci pour ta commande ! </p>
        <img src="/icons/tick.png" alt=""className={css.icon} />
        <p className={css.alert} >Commande n° <span className={css.num_commande} >{props.commande_id}</span> enregistrée !</p>
        <p className={css.memo} >📝 Pense a conserver ton numéro de commande !</p>
        <p className={css.memo} >💬 Pour toute demande d'informations n'hésite pas a contacter <a className={css.link} target="_blank" href="https://www.instagram.com/eliquidebreton/?hl=fr" >@eliquidebreton</a> sur Insta.</p>
      </div>
    )
    }
  </div>
)

export default Enregistree