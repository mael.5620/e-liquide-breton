import React from 'react'
import css from './Aromes.module.css'
import produits from '../../../../../data/produits_specs.json'
import AromeCardMini from './AromesCardMini/AromeCardMini'

const Aromes = (props) => (
  <div className={css.container} >

    {
      produits.aromes.map(item => (
        item.rupture ? null : (
          <AromeCardMini
            image={item.image}
            titre={item.titre}
            handleSelection={props.handleSelection}
            currentSelection={props.currentSelection}
          />
        )
      ))
    }

  </div>
)

export default Aromes