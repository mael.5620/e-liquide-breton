import React, { useState, useEffect } from "react"
import css from './AromeCardMini.module.css'
import classNames from 'classnames'

const AromeCardMini = ({image, titre, handleSelection, currentSelection}) => {
  const [isSelected, setSelected] = useState(currentSelection)
  useEffect(() => currentSelection('arome') === titre ? setSelected(true) : setSelected(false), [currentSelection('arome')])
  return (
    <div className={classNames(css.card_container, isSelected ? css.selected : null)} onClick={() => handleSelection('arome', titre)} >
      <img src={image} alt="" className={css.image} />
      <p className={css.titre}> {titre} </p>
    </div>
  )}

export default AromeCardMini