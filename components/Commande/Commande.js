import React, { Component } from 'react'
import css from './Commande.module.css'
import StepperContainer from './StepperContainer/StepperContainer'
import StepperContainerVertival from './StepperContainer/StepperContainerVertical'

export default class Commande extends Component {
  state = {window_width: undefined}
  componentDidMount() {
    this.setState({window_width: window.innerWidth})
  }

  render() {
    return (
      <div className={css.commande_container} id="commande">
        <p className={css.title} >Passer commande</p>
        {
          this.state.window_width > 600 ? <StepperContainer/> : <StepperContainerVertival />
        }        
      </div>
    )
  }
}