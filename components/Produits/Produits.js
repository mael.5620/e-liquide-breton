import React, { Component } from 'react'
import css from './Produits.module.css'
import produits from '../../data/produits_specs.json'
import BaseCard from "./BaseCard/BaseCard.js"
import AromeCard from "./AromeCard/AromeCard.js"
import TarifCard from './TarifCard/TarifCard'


class Produits extends Component {


  render() {

    return (
      <div className={css.container} id="produits" >
        <div>
          <p className={css.title} >Les bases</p>
          <div className={css.bases_grid} >
            {
              produits.bases.map(item => (
                <BaseCard
                  image={item.image}
                  titre={item.titre}
                  description={item.description}
                  nicotine={item.booster}
                />
              ))
            }
          </div>
        </div>
        <div>
          <p className={css.title} >Les arômes</p>
          <div className={css.aromes_grid} >
            {
              produits.aromes.map(item => (
                <AromeCard
                  image={item.image}
                  titre={item.titre}
                  description={item.description}
                  rupture={item.rupture}
                />
              ))
            }
          </div>
        </div>
        <div>
          <p className={css.title} >Les tarifs</p>
            <div className={css.tarifs_grid} >
              {
                produits.tarifs.map(item => (
                  <TarifCard
                    // volume={item.volume}
                    prix={item.prix}
                    prix_ml={item.prix_ml_disp}
                    image={item.image}
                  />
                ))
              }
            </div>
          <p className={css.sub_title} >Frais d'expédition</p>
          <p>Les envoies par La Poste sont possibles.</p>
          <p className={css.text} ><b>1€</b> pour les commandes d'une fiole de 10ml <br/> 
          <b>2€</b> pour les commandes jusqu'à 50ml (conditionnement : 1x30ml + 2x10ml ou 5x10ml) <br/>
          <b>5€</b> pour les commandes de plus de 50ml (envoie colissimo)</p>

          <p className={css.text} > <br/> Les envoies des commandes jusqu'à 50ml se font en lettre verte dans une enveloppe à bulle.</p>
        </div>
      
      </div>
    )
  }
}


export default Produits