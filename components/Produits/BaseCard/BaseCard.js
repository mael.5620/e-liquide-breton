import React from "react"
import css from './BaseCard.module.css'

const BaseCard = ({image, titre, description, nicotine}) => (
  <div className={css.card_container} >
    <img src={image} alt="" className={css.image} />
    <p className={css.titre}> {titre} </p>
    <p className={css.nicotine} > {nicotine} </p>
    <p className={css.description} > {description} </p>
  </div>
)

export default BaseCard