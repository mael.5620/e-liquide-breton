import React from "react"
import css from './TarifCard.module.css'

const TarifCard = ({prix, prix_ml, image}) => (
  <div className={css.card_container} >
    <img src={image} alt="" className={css.image} />
    {/* <p> {volume} </p> */}
    <p className={css.prix_ml} > {prix_ml} </p>
    <p className={css.prix} > {prix} € </p>
  </div>
)

export default TarifCard