import React from "react"
import css from './AromeCard.module.css'

const AromeCard = ({image, titre, description, rupture}) => (
  <div className={css.card_container} >
    {rupture ? (
      <span className={css.rupture}>
        Rupture
      </span>
    ) : null}
    <img src={image} alt="" className={css.image} />
    <p className={css.titre}> {titre} </p>
    <p className={css.description} > {description} </p>
  </div>
)

export default AromeCard